from viterbi_algorithm.better_main import viterbi
import nltk

states = ['[START]', 'NOUN', 'VERB', '.', 'ADP', 'DET', 'ADJ', 'PRON', 'ADV', 'CONJ', 'PRT', 'NUM', 'X', '[END]']
states_freq = {'NOUN': 270387, 'VERB': 179607,
               '.': 147649, 'ADP': 136406,
               'DET': 125124, 'ADJ': 101722,
               'PRON': 61253, 'ADV': 54972,
               'CONJ': 38371, 'PRT': 30320,
               'NUM': 14438, 'X': 943}

transition_probs = {}
emission_probs = {('[END]', '[END]'): 1,
                  ('[START]', '[START]'): 1}

text = nltk.Text(word.lower() for word in nltk.corpus.brown.words())
tokens = nltk.tag.pos_tag(text, tagset='universal')


tokens_with_states = []
for word, tag in tokens:
    if word == '.':
        tokens_with_states.append((word, tag))
        tokens_with_states.append(('[END]', '[END]'))
        tokens_with_states.append(('[START]', '[START]'))
        continue
    tokens_with_states.append((word, tag))
tokens_with_states = tokens_with_states[:-1]


bigrams = []
for index in range(0, len(tokens_with_states)-1):
    bigrams.append((tokens_with_states[index][1], tokens_with_states[index+1][1]))


all_ = len(bigrams)
for element in bigrams:
    if element in transition_probs:
        continue
    transition_probs[element] = bigrams.count(element)/all_


for word, tag in tokens_with_states:
    if (tag, word) in emission_probs:
        emission_probs[(tag, word)] += 1
    else:
        emission_probs[(tag, word)] = 1

for keys, values in emission_probs.items():
    if keys[0] in states_freq:
        emission_probs[keys] = values/states_freq[keys[0]]

# viterbi('his contention was denied by several bankers', states, transition_probs, emission_probs)
# viterbi('fish sleeps at home', states, transition_probs, emission_probs)
