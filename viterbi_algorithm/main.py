"""
this is simple realisation of Viterbi algorithm...
"""


class ViterbiSequence:
    def __init__(self):
        """
        we have corpus build and we find emission probabilities in accordance with corpus...
        then user inputs some string (two words) and we output a certain tag sequence in accordance to given words;
        e.g. two words: fish sleep --> output: [(noun, 0.7), (verb, 0.8)];
        """
        pass

    @staticmethod
    def sequence_maker_simple_two_words():
        """
        this method is a short implementation which works with sentence structures like: verb + noun | noun + verb
        thus we have two tags: verb and noun;
        sentence length: two words;
        :return: list object consisted of tuples (tag, probability) for given sentence;
        """
        pass


_s = 'fish sleep'

_states = ['[START]', 'noun', 'verb', '[END]']

_trans_probs = {('[START]', 'verb'): 0.2,
                ('[START]', 'noun'): 0.8,
                ('noun', 'verb'): 0.8,
                ('noun', '[END]'): 0.1,
                ('noun', 'noun'): 0.1,
                ('verb', 'noun'): 0.2,
                ('verb', 'verb'): 0.1,
                ('verb', '[END]'): 0.7}

_emis_probs = {('noun', 'fish'): 0.8,
               ('noun', 'sleep'): 0.2,
               ('verb', 'fish'): 0.5,
               ('verb', 'sleep'): 0.5,
               ('[END]', '[END]'): 1,
               ('[START]', '[START]'): 1}


def viterbi(s, states, trans_prob, emis_prob):
    s = s.split()
    s.insert(0, '[START]')
    s.append('[END]')
    vit = []
    bp = {}
    prev_probs = [1, 0, 0, 0]
    for index, word in enumerate(s):
        if index == 0:
            continue
        vit.append([])
        for state in states:
            probs = []
            for _index, _element in enumerate(prev_probs):
                try:
                    transition_probability = trans_prob[(states[_index], state)]
                    emission_probability = emis_prob[(state, s[index])]
                    probs.append(_element * transition_probability * emission_probability)
                except KeyError:
                    probs.append(0)
            vit[-1].append(max(probs))
            if len(vit[-1]) == 4:
                bp[(states[vit[-1].index(max(vit[-1]))], max(vit[-1]))] = \
                    (states[prev_probs.index(max(prev_probs))], max(prev_probs))
        prev_probs = vit[-1]
    vit.insert(0, [1, 0, 0, 0])
    # print(vit)
    # print(bp)
    res = []
    start = '[END]'
    for num in range(len(s)-1, -1, -1):
        for key, value in bp.items():
            if key[0] == start:
                res.append(key)
                start = value[0]
    res.append(('[START]', 1.0))
    ans = []
    for num in range(len(res) - 1, -1, -1):
        ans.append(res[num])
    return ans


# print(viterbi(_s, _states, _trans_probs, _emis_probs))
"""
>>> viterbi(s, states, trans_probs, emis_probs)
fish sleep 
[START] 1.0
noun 0.64[0]1
verb 0.256[0]6
[END] 0.1792[0]3
"""
