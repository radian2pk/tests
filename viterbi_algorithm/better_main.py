_s = 'fish sleep'

_states = ['[START]', 'noun', 'verb', '[END]']

_trans_probs = {('[START]', 'verb'): 0.2,
                ('[START]', 'noun'): 0.8,
                ('noun', 'verb'): 0.8,
                ('noun', '[END]'): 0.1,
                ('noun', 'noun'): 0.1,
                ('verb', 'noun'): 0.2,
                ('verb', 'verb'): 0.1,
                ('verb', '[END]'): 0.7}

_emis_probs = {('noun', 'fish'): 0.8,
               ('noun', 'sleep'): 0.2,
               ('verb', 'fish'): 0.5,
               ('verb', 'sleep'): 0.5,
               ('[END]', '[END]'): 1,
               ('[START]', '[START]'): 1}


def viterbi(s, states, trans_prob, emis_prob):
    s = s.split()
    s.insert(0, '[START]')
    s.append('[END]')
    vit = []
    bp = {}
    prev_probs = []
    for element in states:
        if element == '[START]':
            prev_probs.append(1.0)
            continue
        prev_probs.append(0)

    for index, word in enumerate(s):
        if index == 0:
            continue
        vit.append([])
        for state in states:
            probs = []
            probs_ = {}
            for _index, _element in enumerate(prev_probs):
                try:
                    transition_probability = trans_prob[(states[_index], state)]
                    emission_probability = emis_prob[(state, s[index])]
                    probs.append(_element * transition_probability * emission_probability)
                    v = _element * transition_probability * emission_probability
                except KeyError:
                    probs.append(0)
                    v = 0
                if v:
                    probs_[v] = (_index, _element, state, index, word)
            vit[-1].append(max(probs))
            if max(probs):
                bp[max(probs)] = probs_[max(probs)]
        prev_probs = vit[-1]
    # print(vit)
    print(bp)


# viterbi(_s, _states, _trans_probs, _emis_probs)
"""
>>> viterbi(s, states, trans_probs, emis_probs)
fish sleep 
[START] 1.0
noun 0.64[0]1
verb 0.256[0]6
[END] 0.1792[0]3
"""
