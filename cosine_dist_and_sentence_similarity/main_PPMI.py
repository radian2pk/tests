import math

MATRIX = {'апельсин': {'вкусный': 1,
                       'данные': 0,
                       'компьютер': 0,
                       'результат': 0,
                       'сладкий': 1},
          'информация': {'вкусный': 0,
                         'данные': 6,
                         'компьютер': 1,
                         'результат': 4,
                         'сладкий': 0},
          'цифровой': {'вкусный': 0,
                       'данные': 1,
                       'компьютер': 2,
                       'результат': 1,
                       'сладкий': 0},
          'яблоко': {'вкусный': 1,
                     'данные': 0,
                     'компьютер': 1,
                     'результат': 0,
                     'сладкий': 1}}

COUNTS = 20


class PPMI_PMI:
    def __init__(self):
        pass

    def PPMI(self, word1: str, word2: str, counts=COUNTS) ->float or int:
        prob_w1_w2 = 0
        prob_w1 = 0
        for key, value in MATRIX.items():
            if key == word1:
                for context, prob in value.items():
                    prob_w1 += prob
                    if context == word2:
                        prob_w1_w2 = value[context]/counts
        prob_w1 = prob_w1/counts
        prob_w2 = 0
        for key, value in MATRIX.items():
            for context, prob in value.items():
                if context == word2:
                    prob_w2 += prob
        prob_w2 = prob_w2/counts
        ans = math.log2(prob_w1_w2/(prob_w1*prob_w2))
        return ans

    def PMI(self, word1: str, word2: str) ->float or int:
        ans = self.PPMI(word1, word2)
        if ans > 0:
            return ans
        else:
            return 0

    def counts_add_n(self, n):
        counts = 0
        for key, value in MATRIX.items():
            for context, prob in value.items():
                counts += prob
                counts += n
        return counts

    def PPMI_auto_add2(self, word1: str, word2: str) ->float or int:
        counts = self.counts_add_n(2)
        for main, dct in MATRIX.items():
            for context, prob in dct.items():
                MATRIX[main][context] += 2
        return self.PPMI(word1, word2, counts)


p = PPMI_PMI()
print(p.PPMI('информация', 'данные'))
print(p.PPMI('информация', 'результат'))
print(p.PPMI('информация', 'компьютер'))
print('\n')
print(p.PMI('информация', 'данные'))
print(p.PMI('информация', 'результат'))
print(p.PMI('информация', 'компьютер'))
print('\n')
print(p.PPMI_auto_add2('информация', 'данные'))
print(p.PPMI_auto_add2('информация', 'результат'))
print(p.PPMI_auto_add2('информация', 'компьютер'))
