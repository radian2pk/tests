import math
from collections import Counter
from re import findall


def cosine_sim(v1, v2):
    features = []
    for num, count in v1.items():
        if num not in features:
            features.append(num)

    for num, count in v2.items():
        if num not in features:
            features.append(num)

    sum_mult_counts = 0
    for feature in features:
        try:
            sum_mult_counts += v1[feature] * v2[feature]
        except KeyError:
            continue

    denominator_v1, denominator_v2 = 0, 0
    for num, count in v1.items():
        denominator_v1 += pow(count, 2)
    for num, count in v2.items():
        denominator_v2 += pow(count, 2)
    return sum_mult_counts/(math.sqrt(denominator_v1)*math.sqrt(denominator_v2))


def bag_of_words(sentence: str) ->dict:
    tokens = findall('\w+', sentence.lower())
    bag_words = {}
    for token in tokens:
        if token in bag_words:
            bag_words[token] += 1
        else:
            bag_words[token] = 1
    return bag_words


print(cosine_sim(Counter((1, 0, 0)), Counter((1, 6, 1))))
print(cosine_sim(Counter((0, 1, 2)), Counter((1, 6, 1))))
print(cosine_sim(Counter((1, 0, 0)), Counter((0, 1, 2))))

bow1 = bag_of_words('The cat is on the mat.')
bow2 = bag_of_words('The cat is on the chair.')
bow3 = bag_of_words('I bought a new chair.')

print(cosine_sim(bow1, bow2))
print(cosine_sim(bow1, bow3))
print(cosine_sim(bow2, bow3))
