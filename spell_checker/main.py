"""
Spell Checker "simple" edition
"""
import re
from collections import Counter


class SpellCheck:
    def __init__(self):
        self.list_of_tokens = []
        self.freq_dict = {}
        self.combinations = []

    def get_words(self, text: str) ->list:
        list_of_tokens = re.findall('\w+', text)
        for element in list_of_tokens:
            element.lower()
        self.list_of_tokens = list_of_tokens
        return self.list_of_tokens

    def get_counts(self, tokens: list) -> dict:
        dict_freq = Counter(tokens)
        self.freq_dict = dict_freq
        return self.freq_dict

    def get_edits1(self, word: str) -> list:

        word_combinations = []
        alphabet = 'йцукенгшщзхъфывапролджэёячсмитьбю'

        # one letter deleted
        for num in range(0, len(word)-1):
            word_combinations.append(word[:num]+word[num+1:])
        word_combinations.append(word[:-1])

        # position change
        for num in range(0, len(word)-2):
            word_combinations.append(word[:num]+word[num+1]+word[num]+word[num+2:])
        word_combinations.append(word[:-2]+word[-1]+word[-2])

        # one letter changed with another
        for num in range(0, len(word)):
            for letter in alphabet:
                word_combinations.append(word[:num]+letter+word[num+1:])

        # one letter added
        for num in range(0, len(word)):
            for letter in alphabet:
                word_combinations.append(word[:num+1]+letter+word[num+1:])
        for letter in alphabet:
            word_combinations.append(letter+word)
        self.combinations = word_combinations
        return self.combinations

    @staticmethod
    def get_most_likely(word: str, freq_dict: dict) -> str:
        variants = checker.get_edits1(word)
        list_variants = []
        for element in variants:
            if element in freq_dict:
                list_variants.append((freq_dict[element], element))
        list_variants.sort(reverse=True)
        return list_variants[0][1]


def read_from_file(path_to_file) -> str:
    my_text = ''
    my_file = open(path_to_file, 'r')
    for line in my_file.read():
        my_text += line
    my_file.close()
    return my_text


# checker = SpellCheck()
# text = read_from_file('lifenews2.txt')
# tk = checker.get_words(text)
# dct_ = checker.get_counts(tk)
# print(checker.get_most_likely('ккот', dct_))

