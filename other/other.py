import re

# 1/regular expression that searches for 5-length digits...
test_str = 'lol12345lol*1234)5lol234567'
# print(re.findall("\d\d\d\d\d", test_str))


# 1/simple regular expression that searches for web adresses...
test2_str = 'lol_trash_https://lol_text_fun.com_lol_trash'
# print(re.findall('https?:/[//]\S*[.]{1}\S\S\S?', test2_str))


# 3/simple method that checks wheather password is valid...
tes3_str = 'englang123_Q'


def check_password(password: str) -> bool:
    eng = 'QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm'
    num = '1234567890'
    params = [False, False, False, False]
    if len(password) >= 10:
        params[3] = True
        for letter in password:
            if letter in eng or letter in num or letter == '_':
                if letter.isdigit():
                    params[0] = True
                if letter.isupper():
                    params[1] = True
                if letter.islower():
                    params[2] = True
    if params == [True, True, True, True]: return True 
    else: return False
# print(check_password(tes3_str))

# 4/ simple method that


test4_str = 'uraev.dima@yandex.coms'
# finds email adress
# print(re.findall('^.*@.*\.[.]*[\.]?[com|ru|en]{2,3}', test4_str))
# find domen..
# print(re.findall('@(.*)\.', test4_str))




#Seminar 2...
# 5/
test5_str = "MyVar17 = OtherVar + YetAnother2Var"
test5_str = ' '+test5_str
res = ''
for index in range(0, len(test5_str)):
    if test5_str[index].isupper():
        if test5_str[index-1] == ' ' or test5_str[index-1].isdigit():
            res += test5_str[index].lower()
            continue
        else:
            res += '_'+test5_str[index].lower()
            continue
    res += test5_str[index]
# print(res[1:])

test6_str = """
            В фильме снимались: Л. Мерзин, В. Титова, П. Якоби, У. Росберг, Т. Логинова, В. Басов и другие. 
            В фильме снимались: О. Л. Табаков, А. А. Миронов, А. В. Мишулин, Ю. Толубеев, Е. Евстингеев, Витя Галкин и другие. 
            В фильме снимались: Ира Волкова, Таня Невская, Сережа Кусков, Н. Гвоздикова, А. Харитонов, С. Котикова и другие.
            """

# print(re.findall('([А-Я]\.\s[А-Я][а-я]+[,|\s])|([А-Я]\.\s[А-Я]\.\s[А-Я][а-я]+[,|\s])', test6_str))


