"""
tests for edit_distance_main.py
"""

import unittest
from edit_distance.main import EditDistance


class EditDistanceTest(unittest.TestCase):
    """
    Tests that check different edit_distance cases (with swap)...
     """

    def test_ideal_case_edit_distance(self):
        distance = EditDistance()
        self.assertEqual(distance.get_edit_distance('acre', 'car'), 2)
        self.assertEqual(distance.get_edit_distance('anteater', 'theatre'), 4)
        self.assertEqual(distance.get_edit_distance('banana', 'nanny'), 3)
        self.assertEqual(distance.get_edit_distance('cat', 'crate'), 2)
        self.assertEqual(distance.get_edit_distance('cocoon', 'cuckoo'), 3)
        self.assertEqual(distance.get_edit_distance('emporium', 'empower'), 4)
        self.assertEqual(distance.get_edit_distance('goer', 'ogre'), 2)
        self.assertEqual(distance.get_edit_distance('lyra', 'lay'), 3)
        self.assertEqual(distance.get_edit_distance('life', 'death'), 5)
        self.assertEqual(distance.get_edit_distance('point', 'sirloin'), 5)
        self.assertEqual(distance.get_edit_distance('stone', 'sonnet'), 3)
        self.assertEqual(distance.get_edit_distance('surge', 'ruse'), 3)
        self.assertEqual(distance.get_edit_distance('task', 'tusk'), 1)
        self.assertEqual(distance.get_edit_distance('peat', 'tape'), 4)
        self.assertEqual(distance.get_edit_distance('cast', 'cats'), 1)
        self.assertEqual(distance.get_edit_distance('acts', 'cast'), 2)
        self.assertEqual(distance.get_edit_distance('look', 'book'), 1)
        self.assertEqual(distance.get_edit_distance('book', 'boon'), 1)
        

if __name__ == '__main__':
    unittest.main()
