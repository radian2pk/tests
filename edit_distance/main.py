"""
Edit Distance Calculator (with swap)...
"""


class EditDistance:
    def __init__(self):
        self.matrix = []

    def get_edit_distance(self, w1, w2) ->int:
        matrix = []
        w1 = ' '+w1
        w2 = ' '+w2
        for index in range(0, len(w2)):
            matrix.append([])
            for index_ in range(0, len(w1)):
                matrix[-1].append(0)
        self.matrix = matrix
        
        for second_word in range(0, len(w2)):
            for first_word in range(0, len(w1)):
                if second_word == 0 and first_word == 0:
                    if w1[first_word] != w2[second_word]:
                        self.matrix[0][0] = 1
                elif second_word == 0:
                    self.matrix[0][first_word] = matrix[0][first_word-1] + 1
                elif first_word == 0 and second_word != 0:
                    self.matrix[second_word][0] = matrix[second_word-1][0] + 1
                else:
                    upper = matrix[second_word - 1][first_word]
                    left = matrix[second_word][first_word - 1]
                    last = matrix[second_word - 1][first_word - 1]
                    if w2[second_word-1] == w1[first_word] and w2[second_word] == w1[first_word-1]:
                        if min(upper, left, last) == last:
                            self.matrix[second_word][first_word] += min(upper, left, last)
                        else:
                            self.matrix[second_word][first_word] += min(upper, left, last) + 1
                        continue
                    if w1[first_word] == w2[second_word]:
                        if min(upper, left, last) == last:
                            self.matrix[second_word][first_word] = last
                        else:
                            self.matrix[second_word][first_word] = min(left, upper) + 1
                    else:
                        self.matrix[second_word][first_word] = min(upper, left, last) + 1
        print(self.matrix)
        return self.matrix[-1][-1]

    def get_edit_distance_short(self, w1, w2):
        """
        Not adopted yet...
        :param w1: str
        :param w2: str
        :return: int
        """
        matrix = [[]]
        for index in range(0, len(w1)):
            matrix[0].append(0)
        for index in range(0, len(w2)-1):
            matrix.append(matrix[0])
        self.matrix = matrix

        for second_word in range(0, len(w2)-1):
            for first_word in range(0, len(w1)-1):
                try:
                    upper = self.matrix[second_word-1][first_word]
                except ValueError:
                    upper = None
                try:
                    left = self.matrix[second_word][first_word-1]
                except ValueError:
                    left = None
                try:
                    last = self.matrix[second_word-1][first_word-1]
                except ValueError:
                    last = None

                if w1[first_word] != w2[second_word]:
                    self.matrix[second_word][first_word] = min(upper, left, last) + 1
                else:
                    try:
                        if last == min(upper, left, last):
                            self.matrix[second_word][first_word] = last
                    except ValueError:
                        self.matrix[second_word][first_word] = min(left, upper) + 1


ed = EditDistance()
print(ed.get_edit_distance('banana', 'nanny'))
