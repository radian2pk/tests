"""
Naive Bayes Classifier for film reviews...
"""


features = ['bad', 'poor', 'terrible', 'evil', 'lousy', 'nasty', 'adverse', 'vicious', 'unpleasant', 'defective',
            'corrupted', 'crappy', 'rotten', 'trashy', 'vile', 'wretched', 'miserable', 'unsatisfactory', 'horrible',
            'dreadful', 'inclement', 'foul', 'icky', 'abominable', 'distasteful',
            'good', 'nice', 'pretty', 'fine', 'kind', 'lovely', 'great', 'beautiful', 'happy', 'attractive', 'handsome',
            'cute', 'goodly', 'felicitous', 'well', 'decent', 'proper', 'right', 'positive', 'pleasant', 'satisfactory',
            'sensible', 'acceptable', 'okay', 'worthy']


class BayesClassifier:
    def __init__(self):
        pass

    @staticmethod
    def extract_features(text, features) ->list:
        res = []
        for element in features:
            if element in text:
                res.append(text.count(element))
            else:
                res.append(0)
        return res

    @staticmethod
    def train_nbc(features_, corpora_) ->list:
        res_ = [[], []]
        for feature in features_:
            res_[0].append(corpora_[0].count(feature)+1)
            res_[1].append(corpora_[1].count(feature) + 1)
        res = [[], []]
        for lst in range(len(res_)):
            for feature in res_[lst]:
                res[lst].append(feature/sum(res_[lst]))
        return res

    def classify(self, text, features_, classes_, priors_, params_) ->str:
        ab_prob = self.extract_features(text, features_)
        res = []
        for lst in range(len(classes_)):
            prob = 1
            for feature_freq in range(len(params_[lst])):
                prob *= pow(params_[lst][feature_freq], ab_prob[feature_freq])
            prob *= priors_[lst]
            res.append(prob)
        return classes_[res.index(max(res))]

    @staticmethod
    def evaluate(h: tuple, y: tuple):
        classes = []
        all_classes = [0, 0, 0, 0]
        for index in range(0, len(h)):
            if h[index] in classes:
                continue
            classes.append(h[index])

        if len(classes) == 2:
            for element in classes:
                values = [0, 0, 0, 0]
                for index in range(0, len(h)):
                    if h[index] == element and h[index] == y[index]:
                        values[0] += 1
                    if h[index] == element and h[index] != y[index]:
                        values[1] += 1
                    if h[index] != element and h[index] == y[index]:
                        values[2] += 1
                    if h[index] != element and h[index] != y[index]:
                        values[3] += 1
                part = y.count(element) / len(y)
                acc = (values[0] + values[2]) / sum(values)
                prec = values[0] / (values[0] + values[1])
                rec = values[0] / (values[0] + values[3])
                f_1 = 2 * prec * rec / (prec + rec)
                print('{} \n accuracy {} \n precision {} \n recall {} \n f_1 {}'.format(element, acc, prec, rec, f_1))
                all_classes[0] += acc * part
                all_classes[1] += prec * part
                all_classes[2] += rec * part
                all_classes[3] += f_1 * part
            print('{} \n accuracy {} \n precision {} \n recall {} \n f_1 {}'.format(
                'all', all_classes[0], all_classes[1], all_classes[2], all_classes[3]))
            return

        for element in classes:
            h2 = []
            y2 = []
            for el in h:
                if el == element:
                    h2.append(element)
                else:
                    h2.append(0)
            for el in y:
                if el == element:
                    y2.append(element)
                else:
                    y2.append(0)
            values = [0, 0, 0, 0]
            for index in range(0, len(h2)):
                if h2[index] == element and h2[index] == y2[index]:
                    values[0] += 1
                if h2[index] == element and h2[index] != y2[index]:
                    values[1] += 1
                if h2[index] != element and h2[index] == y2[index]:
                    values[2] += 1
                if h2[index] != element and h2[index] != y2[index]:
                    values[3] += 1
            part = y2.count(element)/len(y2)
            acc = (values[0] + values[2])/sum(values)
            prec = values[0]/(values[0]+values[1])
            rec = values[0]/(values[0]+values[3])
            f_1 = 2*prec*rec/(prec+rec)
            print('{} \n accuracy {} \n precision {} \n recall {} \n f_1 {}'.format(element, acc, prec, rec, f_1))
            all_classes[0] += acc*part
            all_classes[1] += prec*part
            all_classes[2] += rec*part
            all_classes[3] += f_1*part
        print('{} \n accuracy {} \n precision {} \n recall {} \n f_1 {}'.format(
            'all', all_classes[0], all_classes[1], all_classes[2], all_classes[3]))


classifier = BayesClassifier()
'''
corpus_pos = open('pos_train.txt', encoding='utf-8').read()
corpus_neg = open('neg_train.txt', encoding='utf-8').read()
corpora = corpus_pos, corpus_neg
classes = 'positive', 'negative'
params = classifier.train_nbc(features, corpora)
priors = 0.5, 0.5
text1 = 'The movie was horrible, it was absolutely awful.'
label1 = classifier.classify(text1, features, classes, priors, params)
text2 = 'I really enjoyed the film, it was fantastic.'
label2 = classifier.classify(text2, features, classes, priors, params)
# print(label1, label2)  # negative positive
'''

# h = ('pos', 'neg', 'pos', 'pos', 'neg', 'pos', 'neg', 'pos', 'neg', 'pos')
# y = ('pos', 'neg', 'pos', 'neg', 'neg', 'neg', 'neg', 'neg', 'pos', 'neg')
h = (1, 1, 2, 2, 3, 3, 1, 2, 3, 3)
y = (1, 2, 3, 1, 2, 3, 1, 2, 3, 1)
classifier.evaluate(h, y)
# three classes case
# 1
# acc = 0.7
# prec = 0.667
# rec = 0.5
# f1 = 0.571
# all
# acc = 0.67
# prec = 0.517
# rec = 0.5
# f1 = 0.5
