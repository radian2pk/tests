"""
this is short realisation of n-grams...
"""
import re
import math


class NGram:
    @staticmethod
    def tokenize(text: str) ->list:
        return re.findall('\w+', text.lower())

    @staticmethod
    def get_ngram_counts(words: list, n: int) ->dict:
        items = [{}, tuple(words)]
        while n:
            for index in range(len(items[1])-n+1):
                if items[1][index:index+n] in items[0]:
                    items[0][items[1][index:index+n]] += 1
                else:
                    items[0][items[1][index:index+n]] = 1
            n -= 1
        return items[0]

    def get_prob(self, text: str, n_grams: dict, n: int, corp_size: int) ->int or float:
        items = [tuple(text.split()), 1, self.get_ngram_counts(text.split(), n)]
        # print([items[0]])
        if n == 1:
            for key in items[2].keys():
                if len(key) == n:
                    if key in n_grams:
                        items[1] *= n_grams[key] / corp_size
                    else:
                        items[1] *= 0 / corp_size
        else:
            if len(items[0]) < n:
                items[1] = None
            elif len(items[0]) == n:
                if items[0] in n_grams:
                    items[1] = n_grams[items[0]] / n_grams[items[0][:-1]]
                else:
                    items[1] = 0 / n_grams[items[0][:-1]]
            else:
                for key in items[2].keys():
                    if len(key) == n:
                        if key in n_grams:
                            items[1] *= n_grams[key] / n_grams[key[:-1]]
                        else:
                            items[1] *= 0 / n_grams[key[:-1]]
        return items[1]

    def get_prob_log_space(self, text: str, n_grams: dict, n: int, corp_size: int) ->int or float:
        items = [tuple(text.split()), 0, self.get_ngram_counts(text.split(), n)]
        # print([items[0]])
        if n == 1:
            for key in items[2].keys():
                if len(key) == n:
                    if key in n_grams:
                        items[1] += math.log(n_grams[key] / corp_size)
        else:
            if len(items[0]) < n:
                items[1] = None
            elif len(items[0]) == n:
                if items[0] in n_grams:
                    items[1] += math.log(n_grams[items[0]] / n_grams[items[0][:-1]])
            else:
                for key in items[2].keys():
                    if len(key) == n:
                        if key in n_grams:
                            items[1] += math.log(n_grams[key] / n_grams[key[:-1]])
        return items[1]

    def get_prob_add_smoothing(self, text: str, n_grams: dict, n: int, corp_size: int) ->int or float:
        items = [tuple(text.split()), 1, self.get_ngram_counts(text.split(), n), [], 0]
        for key in n_grams.keys():
            if len(key) == n:
                if key not in items[3]:
                    items[3].append(key)
                    items[4] += 1
        # print([items[0]])
        if n == 1:
            for key in items[2].keys():
                if len(key) == n:
                    if key in n_grams:
                        items[1] *= (n_grams[key] + 1) / (corp_size + items[4])
                    else:
                        items[1] *= 1 / (corp_size + items[4])
        else:
            if len(items[0]) < n:
                items[1] = None
            elif len(items[0]) == n:
                if items[0] in n_grams:
                    items[1] = (n_grams[items[0]] + 1) / (n_grams[items[0][:-1]] + items[4])
                else:
                    items[1] = 1/(n_grams[items[0][:-1]] + items[4])
            else:
                for key in items[2].keys():
                    if len(key) == n:
                        if key in n_grams:
                            items[1] *= (n_grams[key] + 1) / (n_grams[key[:-1]] + items[4])
                        else:
                            items[1] *= 1 / (n_grams[key[:-1]] + items[4])
        return items[1]

    def get_prob_both(self, text: str, n_grams: dict, n: int, corp_size: int) ->int or float:
        items = [tuple(text.split()), 0, self.get_ngram_counts(text.split(), n), [], 0]
        for key in n_grams.keys():
            if len(key) == n:
                if key not in items[3]:
                    items[3].append(key)
                    items[4] += 1
        # print([items[0]])
        if n == 1:
            for key in items[2].keys():
                if len(key) == n:
                    if key in n_grams:
                        items[1] += math.log((n_grams[key] + 1) / (corp_size + items[4]))
                    else:
                        items[1] += math.log(1 / (corp_size + items[4]))
        else:
            if len(items[0]) < n:
                items[1] = None
            elif len(items[0]) == n:
                if items[0] in n_grams:
                    items[1] = math.log((n_grams[items[0]] + 1) / (n_grams[items[0][:-1]] + items[4]))
                else:
                    items[1] = math.log(1/(n_grams[items[0][:-1]] + items[4]))
            else:
                for key in items[2].keys():
                    if len(key) == n:
                        if key in n_grams:
                            items[1] += math.log((n_grams[key] + 1) / (n_grams[key[:-1]] + items[4]))
                        else:
                            items[1] += math.log(1 / (n_grams[key[:-1]] + items[4]))
        return items[1]



# scenario
#n_gram = NGram()
#text_ = "I saw a cat and a dog. The cat was sleeping, and the dog was awake. I woke up the cat."
#tk = n_gram.tokenize(text_)
#ngram = n_gram.get_ngram_counts(tk, 5)
#test = ('a cat', 'the cat', 'the dog', 'the woke', 'the cat was awake', 'the cat was dog')
#corp_size_ = len(text_.split())
#for t in test:
  #print(n_gram.get_prob_add_smoothing(t, ngram, 3, corp_size_))

#for t in test:
  #print(n_gram.get_prob(t, ngram, 3, corp_size_))

#for t in test:
  #print(n_gram.get_prob_log_space(t, ngram, 3, corp_size_))

#for t in test:
  #print(n_gram.get_prob_both(t, ngram, 3, corp_size_))
