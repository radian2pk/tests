"""
text generator for n_grams
"""

from n_grams.main import NGram
import random


class Generator:
    def __init__(self):
        f = open('BROWN_PART.txt', 'r', errors='ignore')
        self.corpus = ''
        for line in f.readlines():
            self.corpus += line
        f.close()

        self.n_gram = NGram()
        self.tokens = self.n_gram.tokenize(self.corpus)
        self.counts = self.n_gram.get_ngram_counts(self.tokens, 5)
        self.size = len(self.corpus.split())

    @staticmethod
    def _preparation(full_sequence: str, n: int) ->tuple or str:
        full_sequence = full_sequence.split()
        return ' '.join(full_sequence[:-n+1]), ' '.join(full_sequence[-n+1:])

    def sequence_maker(self, sequence: str, n: int) ->list or str:
        sequence = sequence.split()
        current_sequence = []
        current_grams = []
        for token in self.tokens:
            for el in sequence:
                current_sequence.append(el)
            current_sequence.append(token)
            current_grams.append(current_sequence)
            current_sequence = []

        seq_prob_list = []
        list_grams = []
        for sequence in current_grams:
            if sequence in list_grams:
                continue
            if tuple(sequence) in self.counts:
                list_grams.append(sequence)
                seq_prob_list.append((self.n_gram.get_prob(' '.join(sequence), self.counts, n, self.size),
                                      tuple(sequence)))

        seq_prob_list = sorted(seq_prob_list, reverse=False)
        return seq_prob_list

    def generator(self, string, n):
        safe = string
        list_grams = []
        for token in range(0, len(self.tokens) - 1):
            list_grams.append(string.split())
            list_grams[token].append(self.tokens[token])
            string = safe

        res_list = []
        additional = []
        for seq in list_grams:
            if seq in additional:
                continue
            if tuple(seq) in self.counts:
                prob = self.n_gram.get_prob(' '.join(seq), self.counts, n, self.size)
                res_list.append((prob, seq))
                additional.append(seq)
        return sorted(res_list, reverse=False)

    @staticmethod
    def take_most_frequent(sequences: list) ->str:
        if not sequences:
            return ' '
        return sequences[-1][1]

    @staticmethod
    def take_sample(sequences: list) ->str:
        if not sequences:
            return ' '
        number = random.random()
        sum = 0
        for element in sequences:
            sum += element[0]
            if sum > number:
                return element[1]


generator = Generator()
counter = 20
starting_seq = 'he is going'  # 'library borrower'  # behind this  Artem is going
n = 3
while counter:
    counter -= 1
    parts = generator._preparation(starting_seq, n)
    print(parts[0])
    new_seq = generator.generator(parts[1], n)
    seq_new = generator.take_sample(new_seq)
    starting_seq = ' '.join(seq_new)
print(starting_seq)
