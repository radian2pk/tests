import unittest
from n_grams.main import NGram


"""
		tests for n_gram model and its additions...
		//
"""


class NGramTest(unittest.TestCase):
	def __init__(self, *args, **kwargs):
		super(NGramTest, self).__init__(*args, **kwargs)
		self.n_gram = NGram()
		self.test_text = "I saw a cat and a dog. The cat was sleeping, and the dog was awake. I woke up the cat."
		self.tokens = self.n_gram.tokenize(self.test_text)
		self.counts = self.n_gram.get_ngram_counts(self.tokens, 5)
		self.size = len(self.test_text.split())

	def test_case_get_prob_bi(self):
		self.assertEqual(self.n_gram.get_prob('a cat', self.counts, 2, self.size), 0.5)
		self.assertEqual(self.n_gram.get_prob('the cat', self.counts, 2, self.size), 0.6666666666666666)
		self.assertEqual(self.n_gram.get_prob('the dog', self.counts, 2, self.size), 0.3333333333333333)
		self.assertEqual(self.n_gram.get_prob('the woke', self.counts, 2, self.size), 0.0)
		self.assertEqual(self.n_gram.get_prob('the cat was awake', self.counts, 2, self.size), 0.1111111111111111)
		self.assertEqual(self.n_gram.get_prob('the cat was dog', self.counts, 2, self.size), 0.0)

	def test_case_get_prob_one(self):
		self.assertEqual(self.n_gram.get_prob('a cat', self.counts, 1, self.size), 0.013605442176870746)
		self.assertEqual(self.n_gram.get_prob('the cat', self.counts, 1, self.size), 0.02040816326530612)
		self.assertEqual(self.n_gram.get_prob('the dog', self.counts, 1, self.size), 0.013605442176870746)
		self.assertEqual(self.n_gram.get_prob('the woke', self.counts, 1, self.size), 0.006802721088435373)
		self.assertEqual(self.n_gram.get_prob('the cat was awake', self.counts, 1, self.size), 9.25540284140867e-05)
		self.assertEqual(self.n_gram.get_prob('the cat was dog', self.counts, 1, self.size), 0.0001851080568281734)

	def test_case_get_prob_three(self):
		self.assertEqual(self.n_gram.get_prob('a cat', self.counts, 3, self.size), None)
		self.assertEqual(self.n_gram.get_prob('the cat', self.counts, 3, self.size), None)
		self.assertEqual(self.n_gram.get_prob('the dog', self.counts, 3, self.size), None)
		self.assertEqual(self.n_gram.get_prob('the woke', self.counts, 3, self.size), None)  # ???
		self.assertEqual(self.n_gram.get_prob('the cat was awake', self.counts, 3, self.size), 0.0)
		self.assertEqual(self.n_gram.get_prob('the cat was dog', self.counts, 3, self.size), 0.0)

	def test_case_get_prob_log_space_bi(self):
		self.assertEqual(self.n_gram.get_prob_log_space('a cat', self.counts, 2, self.size), -0.6931471805599453)
		self.assertEqual(self.n_gram.get_prob_log_space('the cat', self.counts, 2, self.size), -0.40546510810816444)
		self.assertEqual(self.n_gram.get_prob_log_space('the dog', self.counts, 2, self.size), -1.0986122886681098)
		self.assertEqual(self.n_gram.get_prob_log_space('the woke', self.counts, 2, self.size), 0) # ???
		self.assertEqual(self.n_gram.get_prob_log_space('the cat was awake', self.counts, 2, self.size), -2.1972245773362196)
		self.assertEqual(self.n_gram.get_prob_log_space('the cat was dog', self.counts, 2, self.size), -1.5040773967762742)

	def test_case_get_prob_log_space_one(self):
		self.assertEqual(self.n_gram.get_prob_log_space('a cat', self.counts, 1, self.size), -4.297285406218791)
		self.assertEqual(self.n_gram.get_prob_log_space('the cat', self.counts, 1, self.size), -3.891820298110627)
		self.assertEqual(self.n_gram.get_prob_log_space('the dog', self.counts, 1, self.size), -4.297285406218791)
		self.assertEqual(self.n_gram.get_prob_log_space('the woke', self.counts, 1, self.size), -4.990432586778736) # ???
		self.assertEqual(self.n_gram.get_prob_log_space('the cat was awake', self.counts, 1, self.size), -9.287717992997527)
		self.assertEqual(self.n_gram.get_prob_log_space('the cat was dog', self.counts, 1, self.size), -8.594570812437581)

	def test_case_get_prob_log_space_three(self):
		self.assertEqual(self.n_gram.get_prob_log_space('a cat', self.counts, 3, self.size), None)
		self.assertEqual(self.n_gram.get_prob_log_space('the cat', self.counts, 3, self.size), None)
		self.assertEqual(self.n_gram.get_prob_log_space('the dog', self.counts, 3, self.size), None)
		self.assertEqual(self.n_gram.get_prob_log_space('the woke', self.counts, 3, self.size), None)
		self.assertEqual(self.n_gram.get_prob_log_space('the cat was awake', self.counts, 3, self.size), -0.6931471805599453)
		self.assertEqual(self.n_gram.get_prob_log_space('the cat was dog', self.counts, 3, self.size), -0.6931471805599453)

	def test_case_get_prob_add_smoothing_bi(self):
		self.assertEqual(self.n_gram.get_prob_add_smoothing('a cat', self.counts, 2, self.size), 0.09523809523809523)
		self.assertEqual(self.n_gram.get_prob_add_smoothing('the cat', self.counts, 2, self.size), 0.13636363636363635)
		self.assertEqual(self.n_gram.get_prob_add_smoothing('the dog', self.counts, 2, self.size), 0.09090909090909091)
		self.assertEqual(self.n_gram.get_prob_add_smoothing('the woke', self.counts, 2, self.size), 0.045454545454545456) 
		self.assertEqual(self.n_gram.get_prob_add_smoothing('the cat was awake', self.counts, 2, self.size), 0.0011806375442739076)
		self.assertEqual(self.n_gram.get_prob_add_smoothing('the cat was dog', self.counts, 2, self.size), 0.0005903187721369538)

	def test_case_get_prob_add_smoothing_one(self):
		self.assertEqual(self.n_gram.get_prob_add_smoothing('a cat', self.counts, 1, self.size), 0.011019283746556474)
		self.assertEqual(self.n_gram.get_prob_add_smoothing('the cat', self.counts, 1, self.size), 0.014692378328741967)
		self.assertEqual(self.n_gram.get_prob_add_smoothing('the dog', self.counts, 1, self.size), 0.011019283746556474)
		self.assertEqual(self.n_gram.get_prob_add_smoothing('the woke', self.counts, 1, self.size), 0.007346189164370983) 
		self.assertEqual(self.n_gram.get_prob_add_smoothing('the cat was awake', self.counts, 1, self.size), 8.094974285808248e-05)
		self.assertEqual(self.n_gram.get_prob_add_smoothing('the cat was dog', self.counts, 1, self.size), 0.0001214246142871237)

	def test_case_get_prob_add_smoothing_three(self):
		self.assertEqual(self.n_gram.get_prob_add_smoothing('a cat', self.counts, 3, self.size), None)
		self.assertEqual(self.n_gram.get_prob_add_smoothing('the cat', self.counts, 3, self.size), None)
		self.assertEqual(self.n_gram.get_prob_add_smoothing('the dog', self.counts, 3, self.size), None)
		self.assertEqual(self.n_gram.get_prob_add_smoothing('the woke', self.counts, 3, self.size), None) 
		self.assertEqual(self.n_gram.get_prob_add_smoothing('the cat was awake', self.counts, 3, self.size), 0.004761904761904762)
		self.assertEqual(self.n_gram.get_prob_add_smoothing('the cat was dog', self.counts, 3, self.size), 0.004761904761904762)

	def test_case_get_prob_add_both_bi(self):
		self.assertEqual(self.n_gram.get_prob_both('a cat', self.counts, 2, self.size), -2.3513752571634776)
		self.assertEqual(self.n_gram.get_prob_both('the cat', self.counts, 2, self.size), -1.9924301646902063)
		self.assertEqual(self.n_gram.get_prob_both('the dog', self.counts, 2, self.size), -2.3978952727983707)
		self.assertEqual(self.n_gram.get_prob_both('the woke', self.counts, 2, self.size), -3.0910424533583156) 
		self.assertEqual(self.n_gram.get_prob_both('the cat was awake', self.counts, 2, self.size), -6.741700694652055)
		self.assertEqual(self.n_gram.get_prob_both('the cat was dog', self.counts, 2, self.size), -7.434847875212)

	def test_case_get_prob_add_both_one(self):
		self.assertEqual(self.n_gram.get_prob_both('a cat', self.counts, 1, self.size), -4.50810847314496)
		self.assertEqual(self.n_gram.get_prob_both('the cat', self.counts, 1, self.size), -4.220426400693179)
		self.assertEqual(self.n_gram.get_prob_both('the dog', self.counts, 1, self.size), -4.50810847314496)
		self.assertEqual(self.n_gram.get_prob_both('the woke', self.counts, 1, self.size), -4.913573581253124) 
		self.assertEqual(self.n_gram.get_prob_both('the cat was awake', self.counts, 1, self.size), -9.421682054398085)
		self.assertEqual(self.n_gram.get_prob_both('the cat was dog', self.counts, 1, self.size), -9.01621694628992)

	def test_case_get_prob_add_both_three(self):
		self.assertEqual(self.n_gram.get_prob_both('a cat', self.counts, 3, self.size), None)
		self.assertEqual(self.n_gram.get_prob_both('the cat', self.counts, 3, self.size), None)
		self.assertEqual(self.n_gram.get_prob_both('the dog', self.counts, 3, self.size), None)
		self.assertEqual(self.n_gram.get_prob_both('the woke', self.counts, 3, self.size), None) 
		self.assertEqual(self.n_gram.get_prob_both('the cat was awake', self.counts, 3, self.size), -5.3471075307174685)
		self.assertEqual(self.n_gram.get_prob_both('the cat was dog', self.counts, 3, self.size), -5.3471075307174685)


if __name__ == '__main__':
	unittest.main()
