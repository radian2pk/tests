from main import NGram

n_gram = NGram()

f = open('BROWN_ALL.txt', 'r', errors='ignore')
corpus = ''
for line in f.readlines():
    corpus += line
f.close()

tokens_ = n_gram.tokenize(corpus)
counts_ = n_gram.get_ngram_counts(tokens_, 2)
starting_sequence = 'snake'
corpus_size = len(corpus.split())


def generator(string, counts, tokens, corpus_size_):
    list_grams = []
    for token in tokens:
        list_grams.append([string, token])

    res_list = []
    for seq in list_grams:
        if tuple(seq) in counts:
            prob = n_gram.get_prob(' '.join(seq), counts, 2, corpus_size_)
            res_list.append((prob, seq))
    return sorted(res_list, reverse=True)[0][1][1]


counter = 10
print(starting_sequence)
while counter:
    counter -= 1
    res = generator(starting_sequence, counts_, tokens_, corpus_size)
    print(res)
    starting_sequence = res
