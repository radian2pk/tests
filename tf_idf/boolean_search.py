"""
tf_idf
"""

from pymystem3 import Mystem
m = Mystem()


def all_df(*args):
    """
    using lemmas
    :param args: words to find
    :return: dict of each lemma with its boolean id in each doc
    """
    boolean_search_ = {}
    for word in args:
        boolean_search_[m.lemmatize(word)[0]] = []
    for i in range(1, 102):
        with open(file='/Users/dmitry-mac/Programming/tests/tf_idf/nlp_6/задание 6 news_lem/_lem({}).txt'.format(i)) as f:
            corpus = ''
            for line in f.readlines():
                corpus += line
            f.close()
            for word in boolean_search_.keys():
                if word in corpus:
                    boolean_search_[word].append(1)
                else:
                    boolean_search_[word].append(0)
    return boolean_search_


def boolean_search(df_all, *query):
    for qr in query:
        print(qr)
        tokens = qr.split()
        all = []
        for token in tokens:
            lemmatised = m.lemmatize(token)[0]
            ans = []
            for element in range(0, len(df_all[lemmatised])):
                if df_all[lemmatised][element]:
                    ans.append(element+1)
            all.append(ans)
        positive = False
        for el in all[0]:
            for lists in all:
                if el in lists:
                    positive = True
                else:
                    positive = False
                    break
            if positive:
                print(el)
    return


words = all_df('путин', 'трамп', 'обама', 'япония', 'курильские', 'острова', 'губка', 'боб', 'квадратные', 'штаны')
boolean_search(words, 'путин трамп', 'путин обама', 'япония курильские острова', 'губка боб квадратные штаны')
