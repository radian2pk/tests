from pymystem3 import Mystem
from re import findall
from math import log
m = Mystem()

N = 101


def read_adapter():
    corpus = []
    for i in range(1, 102):
        with open(file='/Users/dmitry-mac/Programming/tests/tf_idf/nlp_6/задание 6 news_lem/_lem({}).txt'.format(i)) as f:
            corpus.append(f.read())
            f.close()
    return corpus


def tokenize(corpus, unique=True):
    uniq = {}
    if unique:
        for text in corpus:
            text_tokens = findall('\w+', text)
            for token in text_tokens:
                if token not in uniq:
                    uniq[token] = 0
        return corpus, uniq
    return corpus


def df_count(unique_tokens_dict, all_corpus_list):
    for token in unique_tokens_dict:
        for text in all_corpus_list:
            if token in text:
                unique_tokens_dict[token] += 1
    return unique_tokens_dict


def idf_count_each(uniq_tokens_df):
    idf_each_token = {}
    for token, df in uniq_tokens_df.items():
        idf_each_token[token] = log(N/df)
    return idf_each_token


"""PART1 - IDF for each unique term in collection"""
crp = read_adapter()
all_, unique_ = tokenize(crp)
tokens_df = df_count(unique_, all_)
idf_each = idf_count_each(tokens_df)


def work_with_query(q: str, idf_dct: dict, corpus_all_texts_list: list):
    q = findall('\w+', q.lower())
    q_lem = []
    for token in q:
        q_lem.append(m.lemmatize(token)[0])
    # print(q_lem)

    couter_text = 0
    result_list = []
    for text_str in corpus_all_texts_list:
        couter_text += 1
        sum_tf_idf_query = 0
        for each_term in q_lem:
            try:
                tf = 1 + log(findall('\w+', text_str).count(each_term))
            except ValueError:
                tf = 1
            sum_tf_idf_query += tf*idf_dct[each_term]
        result_list.append((sum_tf_idf_query, couter_text))
    print(sorted(result_list, reverse=True))


"""PART2 - tf-idf for query"""
query = 'путин трамп'
work_with_query(query, idf_each, crp)
